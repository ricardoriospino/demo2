package libreria.bean;
import java.io.Serializable;

public class LibroBean implements Serializable {
	
	private Long id;
	private Long idAutor;
	private String titulo;
	private int a�o;
	private String idioma;
	private String url;
	private Double precio;
	private int vendible;
	private int copias;
	private String descripcion; 
	
	public LibroBean() {
		
	}

	public LibroBean(Long id, Long idAutor, String titulo, int a�o, String idioma, String url, Double precio,
			int vendible, int copias, String descripcion) {
		super();
		this.id = id;
		this.idAutor = idAutor;
		this.titulo = titulo;
		this.a�o = a�o;
		this.idioma = idioma;
		this.url = url;
		this.precio = precio;
		this.vendible = vendible;
		this.copias = copias;
		this.descripcion = descripcion;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdAutor() {
		return idAutor;
	}

	public void setIdAutor(Long idAutor) {
		this.idAutor = idAutor;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getA�o() {
		return a�o;
	}

	public void setA�o(int a�o) {
		this.a�o = a�o;
	}

	public String getIdioma() {
		return idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	public int getVendible() {
		return vendible;
	}

	public void setVendible(int vendible) {
		this.vendible = vendible;
	}

	public int getCopias() {
		return copias;
	}

	public void setCopias(int copias) {
		this.copias = copias;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
	

}
