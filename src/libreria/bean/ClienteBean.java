package libreria.bean;

import java.io.Serializable;
import java.util.Date;

public class ClienteBean implements Serializable {
	
	private Long id;
	private String nombre;
	private String email;
	private Date fechaDeNacimiento ;
	private String genero;
	private int activo;
	private Date fechaCreacion;
	
	public ClienteBean() {
		
	}
		
	public ClienteBean(Long id, String nombre, String email, Date fechaDeNacimiento, String genero, int activo,
			Date fechaCreacion) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.email = email;
		this.fechaDeNacimiento = fechaDeNacimiento;
		this.genero = genero;
		this.activo = activo;
		this.fechaCreacion = fechaCreacion;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public Date getFechaDeNacimiento() {
		return fechaDeNacimiento;
	}


	public void setFechaDeNacimiento(Date fechaDeNacimiento) {
		this.fechaDeNacimiento = fechaDeNacimiento;
	}


	public String getGenero() {
		return genero;
	}


	public void setGenero(String genero) {
		this.genero = genero;
	}


	public int getActivo() {
		return activo;
	}


	public void setActivo(int activo) {
		this.activo = activo;
	}


	public Date getFechaCreacion() {
		return fechaCreacion;
	}


	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}


}
