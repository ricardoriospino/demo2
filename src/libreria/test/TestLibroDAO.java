package libreria.test;
import java.util.List;

import libreria.bean.ClienteBean;
import libreria.bean.LibroBean;
import libreria.dao.LibroDAO;
import libreria.dao.LibroDAOimpl;

public class TestLibroDAO {

	public static void main(String[] args) {
		
		LibroDAO libroDAO = new LibroDAOimpl();
		
		//listar todos los clientes 
		List<LibroBean> lista = libroDAO.listarLibros();
		
		for(LibroBean libro : lista) {
			
			System.out.println("libro:" + libro.getId() + "  idAutor:" + libro.getIdAutor() + " title:" + libro.getTitulo() 
			
			+ " a�o: " + libro.getA�o() + "  idioma:" + libro.getIdioma()
			+ "  url: "+ libro.getUrl() + " precio : " + libro.getPrecio()
			+ "  vendible: " + libro.getVendible() + "  copias:" + libro.getCopias()
			+ " descripcion: " + libro.getDescripcion());
			
			
		}
		
		//insertar libro
		
			//LibroBean libronuevo = new LibroBean(199L,1L,"Laguna Vieja",2020,"es",null,10.09,1,100,"La laguna encantada en Per�");
			//libroDAO.insertarLibror(libronuevo);
		
		//eliminar libro
			//libroDAO.eliminarLibro(200L);
		
		//actualizar
		
		LibroBean libro = new LibroBean(198L,12L,"La cura",2020,"es",null,1.01,2,40,"muerto");
		libroDAO.actualizarLibro(libro);
		

	}

}
