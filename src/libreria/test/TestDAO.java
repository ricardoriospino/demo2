package libreria.test;

import java.util.List;
import libreria.bean.AutorBean;
import libreria.dao.AutorDAO;
import libreria.dao.AutorDAOimpl;

public class TestDAO {

	public static void main(String[] args) {
	
		AutorDAO autorDAO = new AutorDAOimpl();
		
		//listar todos los autores 
		List<AutorBean> lista = autorDAO.listarAutores();
		
		for(AutorBean autor :  lista) {
			System.out.println("autor:" + autor.getId() + "  nombre:" + autor.getNombre() + "  pais:" + autor.getNacionalidad());
			
		}

		
		/*
		 String pais= "USA";
		List<AutorBean> lista = autorDAO.lisAutorPorPais(pais);
		
		for(AutorBean autor :  lista) {
			System.out.println("autor:" + autor.getId() + "  nombre:" + autor.getNombre() + "  pais:" + autor.getNacionalidad());
		
		}*/
		
		// insertar autor 
	//	AutorBean autornuevo = new AutorBean(null,"BATMAN", "PER");
	// autorDAO.insertarAutor(autornuevo);
		
		
		
		//eliminar autor 
		//autorDAO.eliminarAutor(194L);
		
		
		// ACTUALIZAR 
//		AutorBean autor = new AutorBean(1930L,"firu","COL");
//		autorDAO.actualizarAutor(autor);
		
		

		

	}

}
