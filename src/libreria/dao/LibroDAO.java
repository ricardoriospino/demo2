package libreria.dao;
import java.util.List;
import libreria.bean.LibroBean;

public interface LibroDAO {



	public List<LibroBean>listarLibros();
	public List<LibroBean>lisLibroPorA�o(int a�o);
	
	public int insertarLibror (LibroBean libro);
	public int eliminarLibro (Long idLibro);
	public int actualizarLibro (LibroBean libro);
	

}

