package libreria.dao;

import java.util.Date;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;


import libreria.bean.ClienteBean;
import libreria.db.MiConexion;

public class ClienteDAOimpl implements ClienteDAO {

	@Override
	public List<ClienteBean> listaClientes() {
		
	
		List<ClienteBean>lst = new ArrayList<ClienteBean>();
		Connection cnx = null;
		PreparedStatement pst =null;
		ResultSet rs =null;
		try {
			cnx = new MiConexion().getConexion();
			String sql="select * from clients";
			
			pst = (PreparedStatement) cnx.prepareStatement(sql);
			pst.execute();
			rs= pst.getResultSet();
			
			ClienteBean cliente =null;
			
			while(rs.next()) {
				
				cliente = new ClienteBean();
				cliente.setId(rs.getLong("client_id"));
				cliente.setNombre(rs.getString("name"));
				cliente.setEmail(rs.getString("email"));
				cliente.setFechaDeNacimiento(rs.getDate("birthdate"));
				cliente.setGenero(rs.getString("gender"));
				cliente.setActivo(rs.getInt("active"));
				cliente.setFechaCreacion(rs.getDate("created_at"));
				
				lst.add(cliente);
				
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return lst;
	}

	@Override
	public List<ClienteBean> litarPorGenero(String genero) {
		
		List<ClienteBean>lst = new ArrayList<ClienteBean>();
		Connection cnx = null;
		PreparedStatement pst =null;
		ResultSet rs =null;
		try {
			cnx = new MiConexion().getConexion();
			String sql="select * from clients  WHERE gender = ? ";
			
			pst = (PreparedStatement) cnx.prepareStatement(sql);
			pst.execute();
			rs= pst.getResultSet();
			
			ClienteBean cliente =null;
			
			while(rs.next()) {
				
				cliente = new ClienteBean();
				cliente.setId(rs.getLong("client_id"));
				cliente.setNombre(rs.getString("name"));
				cliente.setEmail(rs.getString("email"));
				cliente.setFechaDeNacimiento(rs.getDate("birthdate"));
				cliente.setGenero(rs.getString("gender"));
				cliente.setActivo(rs.getInt("active"));
				cliente.setFechaCreacion(rs.getDate("created_at"));
				
				lst.add(cliente);
				
			}
		}catch (Exception e) {
			e.printStackTrace();
		
		}			
		return lst;	
		
	}

	@Override
	public int insertarCliente(ClienteBean cliente) {
		
		Connection cnx =null;
		PreparedStatement pst = null;
		int registros = 0;
		
		try{
			
			//Paso 1: Crear La Conexion
			cnx = new MiConexion().getConexion(); 
			
			//Paso 2: Preparamos la sentencia SQL
			 
			String sql ="INSERT into clients (name,email,birthdate,gender,active,created_at)"
					+ "values(?,?,?,?,?,?)";
			pst = (PreparedStatement) cnx.prepareStatement(sql);
			pst.setString(1, cliente.getNombre());
			pst.setString(2, cliente.getEmail());
			pst.setDate(3,convertir(cliente.getFechaDeNacimiento()));
			pst.setString(4, cliente.getGenero());
			pst.setInt(5,cliente.getActivo());
			pst.setDate(6, null);
			
			
			//paso 3: Enviamos sentencia SQL a la base de datos
			registros = pst.executeUpdate();//para insert , update,delete
			
			System.out.println("Registros Insertados: "+ registros);
	
		
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			
			try{
				//cerrar la conexion a la base de datos
				if(pst !=null) pst.close();
				if(cnx !=null) cnx.close();
			
			}catch (Exception e) {
				e.printStackTrace();
			}	
		}
		
		return registros;
		
		
	}

	@Override
	public int eliminarCliente(Long idCliente) {
		Connection cnx =null;
		PreparedStatement pst = null;
		int registros = 0;

		try{

			//Paso 1: Crear La Conexion
			cnx = new MiConexion().getConexion(); 
			
			//Paso 2: Preparamos la sentencia SQL
			String sql ="delete from clients where client_id = ?";
			pst = (PreparedStatement) cnx.prepareStatement(sql);
			pst.setLong(1, idCliente);
			
			//paso 3: Enviamos sentencia SQL a la base de datos
			registros = pst.executeUpdate();//para insert , update,delete
			
			System.out.println("Registros eliminados: "+registros);
			
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			
			try{
				//cerrar la conexion a la base de datos
				if(pst !=null) pst.close();
				if(cnx !=null) cnx.close();
			
			}catch (Exception e) {
				e.printStackTrace();
			}	
		}
		
		return registros;
			
	}

	@Override
	public int actualizarCliente(ClienteBean cliente) {
		
			Connection cnx =null;
			PreparedStatement pst = null;
			int registros = 0;	
			try{
				//Paso 1: Crear La Conexion
				cnx = new MiConexion().getConexion(); 
				
				//Paso 2: Preparamos la sentencia SQL
				String sql ="update clients " + 
						"set name = ?, gender = ?" + 
						",birthdate= ?" +
						" WHERE client_id = ?";
			
				
				System.out.println(sql);
				pst = (PreparedStatement) cnx.prepareStatement(sql);
				pst.setString(1, cliente.getNombre());
				pst.setString(2, cliente.getGenero());
				pst.setDate(3, convertir(cliente.getFechaDeNacimiento()));
				pst.setLong(4, cliente.getId());
			
				
				//paso 3: Enviamos sentencia SQL a la base de datos
				registros = pst.executeUpdate();//para insert , update,delete
				
				System.out.println("Registros actualizados: "+ registros);
				
				
			}catch (Exception e) {
				e.printStackTrace();

			}

			return registros;
	}
	
	// convirte de java.util.date a java.sql.date
	
		private  java.sql.Date convertir(java.util.Date uDate) {
			java.sql.Date sDate = new java.sql.Date(uDate.getTime());
			return sDate;
		}
	

}
