package libreria.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import libreria.bean.AutorBean;
import libreria.bean.LibroBean;
import libreria.db.MiConexion;

public class LibroDAOimpl implements LibroDAO {

	@Override
	public List<LibroBean> listarLibros() {
		
		List<LibroBean>lst = new ArrayList<LibroBean>();
		Connection cnx = null;
		PreparedStatement pst =null;
		ResultSet rs =null;
		try {
			cnx = new MiConexion().getConexion();
			String sql="select * from books";
			
			pst = (PreparedStatement) cnx.prepareStatement(sql);
			pst.execute();
			rs= pst.getResultSet();
			
			LibroBean libro =null;
			
			while(rs.next()) {
				
				libro = new LibroBean();
				libro.setId(rs.getLong("book_id"));
				libro.setIdAutor(rs.getLong("author_id"));
				libro.setTitulo(rs.getString("title"));
				libro.setA�o(rs.getInt("year"));
				libro.setIdioma(rs.getString("language"));
				libro.setUrl(rs.getString("cover_url"));
				libro.setPrecio(rs.getDouble("price"));
				libro.setVendible(rs.getInt("sellable"));
				libro.setCopias(rs.getInt("copies"));
				libro.setDescripcion(rs.getString("description"));
				
				lst.add(libro);
				
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return lst;
		
	}

	@Override
	public List<LibroBean> lisLibroPorA�o(int a�o) {
		
		List<LibroBean>lst = new ArrayList<LibroBean>();
		Connection cnx = null;
		PreparedStatement pst =null;
		ResultSet rs =null;
		try {
			cnx = new MiConexion().getConexion();
			String sql="select * from books WHERE year = ? ";
			
			pst = (PreparedStatement) cnx.prepareStatement(sql);
			pst.execute();
			rs= pst.getResultSet();
			
			LibroBean libro =null;
			
			while(rs.next()) {
				
				libro = new LibroBean();
				libro.setId(rs.getLong("book_id"));
				libro.setIdAutor(rs.getLong("author_id"));
				libro.setTitulo(rs.getString("title"));
				libro.setA�o(rs.getInt("year"));
				libro.setIdioma(rs.getString("language"));
				libro.setUrl(rs.getString("cover_url"));
				libro.setPrecio(rs.getDouble("price"));
				libro.setVendible(rs.getInt("sellable"));
				libro.setCopias(rs.getInt("copies"));
				libro.setDescripcion(rs.getString("description"));
				
				lst.add(libro);
				
			}
		}catch (Exception e) {
			e.printStackTrace();
		
		}			
		return lst;	
		
	}

	@Override
	public int insertarLibror(LibroBean libro) {
		
		Connection cnx =null;
		PreparedStatement pst = null;
		int registros = 0;
		
		try{
			
			//Paso 1: Crear La Conexion
			cnx = new MiConexion().getConexion(); 
			
			//Paso 2: Preparamos la sentencia SQL
			 
			String sql ="INSERT into books (author_id,title,year,language,cover_url,price,sellable,copies,description)"
					+ "values(?,?,?,?,?,?,?,?,?)";
			pst = (PreparedStatement) cnx.prepareStatement(sql);
			pst.setLong(1, libro.getIdAutor());
			pst.setString(2, libro.getTitulo());
			pst.setInt(3, libro.getA�o());
			pst.setString(4, libro.getIdioma());
			pst.setString(5, libro.getUrl());
			pst.setDouble(6, libro.getPrecio());
			pst.setInt(7, libro.getVendible());
			pst.setInt(8, libro.getCopias());
			pst.setString(9, libro.getDescripcion());
			
			//paso 3: Enviamos sentencia SQL a la base de datos
			registros = pst.executeUpdate();//para insert , update,delete
			
			System.out.println("Registros Insertados: "+ registros);
	
		
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			
			try{
				//cerrar la conexion a la base de datos
				if(pst !=null) pst.close();
				if(cnx !=null) cnx.close();
			
			}catch (Exception e) {
				e.printStackTrace();
			}	
		}
		
		return registros;
		
	}

	@Override
	public int eliminarLibro(Long idLibro) {
		
		Connection cnx =null;
		PreparedStatement pst = null;
		int registros = 0;

		try{

			//Paso 1: Crear La Conexion
			cnx = new MiConexion().getConexion(); 
			
			//Paso 2: Preparamos la sentencia SQL
			String sql ="delete from books where book_id = ?";
			pst = (PreparedStatement) cnx.prepareStatement(sql);
			pst.setLong(1, idLibro);
			
			//paso 3: Enviamos sentencia SQL a la base de datos
			registros = pst.executeUpdate();//para insert , update,delete
			
			System.out.println("Registros eliminados: "+registros);
			
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			
			try{
				//cerrar la conexion a la base de datos
				if(pst !=null) pst.close();
				if(cnx !=null) cnx.close();
			
			}catch (Exception e) {
				e.printStackTrace();
			}	
		}
		
		return registros;
			
	
	}

	@Override
	public int actualizarLibro(LibroBean libro) {
		Connection cnx =null;
		PreparedStatement pst = null;
		int registros = 0;	
		try{
			//Paso 1: Crear La Conexion
			cnx = new MiConexion().getConexion(); 
			
			//Paso 2: Preparamos la sentencia SQL
			String sql ="update books " + 
					"set title = ?, year = ?" + 
					" WHERE book_id = ?";
			
			System.out.println(sql);
			pst = (PreparedStatement) cnx.prepareStatement(sql);
			pst.setString(1, libro.getTitulo());
			pst.setInt(2, libro.getA�o());
			pst.setLong(3, libro.getId());
		
			
			//paso 3: Enviamos sentencia SQL a la base de datos
			registros = pst.executeUpdate();//para insert , update,delete
			
			System.out.println("Registros actualizados: "+ registros);
			
			
		}catch (Exception e) {
			e.printStackTrace();

		}

		return registros;

	}

}
