package libreria.dao;

import java.util.List;
import libreria.bean.ClienteBean;


public interface ClienteDAO {
	public List<ClienteBean>listaClientes();
	public List<ClienteBean>litarPorGenero(String genero);
	
	public int insertarCliente (ClienteBean cliente);
	public int eliminarCliente (Long idCliente);
	public int actualizarCliente ( ClienteBean cliente);
	

}
