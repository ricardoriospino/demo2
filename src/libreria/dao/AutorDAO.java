package libreria.dao;
import java.util.List;

import libreria.bean.AutorBean;

public interface AutorDAO {
	
	public List<AutorBean>listarAutores();
	public List<AutorBean>lisAutorPorPais(String pais);
	
	public int insertarAutor (AutorBean autor);
	public int eliminarAutor (Long idAutor);
	public int actualizarAutor (AutorBean autor);
	

}
