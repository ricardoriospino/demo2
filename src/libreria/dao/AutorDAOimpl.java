package libreria.dao;

import java.sql.Connection; // agregado 
import java.sql.PreparedStatement; // agregado 
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

//import com.mysql.jdbc.Connection; // estamos pasando a pool de conexiones 
//import com.mysql.jdbc.PreparedStatement;
import libreria.bean.AutorBean;
import libreria.db.MiConexion;
import libreria.db.PoolConexiones;

public class AutorDAOimpl implements AutorDAO {

	@Override
	public List<AutorBean> listarAutores() {
		
		List<AutorBean>lst = new ArrayList<AutorBean>();
		 Connection cnx = null;	
		PreparedStatement pst =null;
		ResultSet rs =null;
		try {
			
			//cnx = new MiConexion().getConexion();
			cnx= PoolConexiones.getConexion();
			String sql="select * from authors";
			
			pst = (PreparedStatement) cnx.prepareStatement(sql);
			pst.execute();
			rs= pst.getResultSet();
			
			AutorBean autor =null;
			
			while(rs.next()) {
				
				autor = new AutorBean();
				autor.setId(rs.getLong("author_id"));
				autor.setNacionalidad(rs.getString("nationality"));
				autor.setNombre(rs.getString("name"));
				
				lst.add(autor);
				
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return lst;
	}

	@Override
	public List<AutorBean> lisAutorPorPais(String pais) {
		
			List<AutorBean>lst = new ArrayList<AutorBean>();
			Connection cnx = null;
			PreparedStatement pst =null;
			ResultSet rs =null;
			try {
				cnx = new MiConexion().getConexion();
				String sql="select * from authors WHERE nationality = ? ";
				
				pst = (PreparedStatement) cnx.prepareStatement(sql);
				pst.execute();
				rs= pst.getResultSet();
				
				AutorBean autor =null;
				
				while(rs.next()) {
					
					autor = new AutorBean();
					autor.setId(rs.getLong("author_id"));
					autor.setNacionalidad(rs.getString("nationality"));
					autor.setNombre(rs.getString("name"));
					
					lst.add(autor);
					
				}
			}catch (Exception e) {
				e.printStackTrace();
			
			}			
			return lst;	
		
		// tarea de libros y clientes 
		// listar por pais ejemplo usa 	
	}

	@Override
	public int insertarAutor(AutorBean autor) {
		
		Connection cnx =null;
		PreparedStatement pst = null;
		int registros = 0;
		
		try{
			
			//Paso 1: Crear La Conexion
			//cnx = new MiConexion().getConexion(); 
			cnx= PoolConexiones.getConexion();
			
			//Paso 2: Preparamos la sentencia SQL
			String sql ="INSERT into authors (name,nationality)"
					+ "values(?,?)";
			pst = (PreparedStatement) cnx.prepareStatement(sql);
			pst.setString(1, autor.getNombre());
			pst.setString(2, autor.getNacionalidad());
		
			
			//paso 3: Enviamos sentencia SQL a la base de datos
			registros = pst.executeUpdate();//para insert , update,delete
			
			System.out.println("Registros Insertados: "+ registros);
	
			
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			
			try{
				//cerrar la conexion a la base de datos
				if(pst !=null) pst.close();
				if(cnx !=null) cnx.close();
			
			}catch (Exception e) {
				e.printStackTrace();
			}	
		}
		
		return registros;
		
	
	}

	@Override
	public int eliminarAutor(Long idAutor) {
		Connection cnx =null;
		PreparedStatement pst = null;
		int registros = 0;

		try{

			//Paso 1: Crear La Conexion
			cnx = new MiConexion().getConexion(); 
			
			//Paso 2: Preparamos la sentencia SQL
			String sql ="delete from authors where author_id = ?";
			pst = (PreparedStatement) cnx.prepareStatement(sql);
			pst.setLong(1, idAutor);
			
			//paso 3: Enviamos sentencia SQL a la base de datos
			registros = pst.executeUpdate();//para insert , update,delete
			
			System.out.println("Registros eliminados: "+registros);
			
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			
			try{
				//cerrar la conexion a la base de datos
				if(pst !=null) pst.close();
				if(cnx !=null) cnx.close();
			
			}catch (Exception e) {
				e.printStackTrace();
			}	
		}
		
		return registros;
		
	
	}

	@Override
	public int actualizarAutor(AutorBean autor) {
		
		Connection cnx =null;
		PreparedStatement pst = null;
		int registros = 0;	
		try{
			//Paso 1: Crear La Conexion
			cnx = new MiConexion().getConexion(); 
			
			//Paso 2: Preparamos la sentencia SQL
			String sql ="update authors " + 
					"set name = ?, nationality = ?" + 
					" WHERE author_id = ?";
			
			System.out.println(sql);
			pst = (PreparedStatement) cnx.prepareStatement(sql);
			pst.setString(1, autor.getNombre());
			pst.setString(2, autor.getNacionalidad());
			pst.setLong(3, autor.getId());
		
			
			//paso 3: Enviamos sentencia SQL a la base de datos
			registros = pst.executeUpdate();//para insert , update,delete
			
			System.out.println("Registros actualizados: "+registros);
			
			
		}catch (Exception e) {
			e.printStackTrace();

		}

		return registros;
	}

}
