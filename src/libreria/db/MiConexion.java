package libreria.db;

// generar una tabla en mysql

// gestor de registros : public int insertLibro (LibroBean producto)

import java.sql.DriverManager;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Driver;

public class MiConexion {
	
	static {
		
		try {
			// invocar a las clases el driver 
			Class.forName("com.mysql.jdbc.Driver");
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}
	// parte del driver, pinchar en el error 
	
	//IP 127.0.0.1
	//port 3306
	// name root
	// clave mysql
	
	public Connection getConexion() {
		Connection cnx = null;
		
		try {
			// necesitaba un casteo dando click al error 
			cnx = (Connection) DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/academico_en?useSSL=false",
					
					//solamente cambias bd_java
					"root",
					"mysql"); 
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cnx;
		
	}

}
